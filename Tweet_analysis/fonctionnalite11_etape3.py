import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError


def get_tweets_from_candidates_search_queries(num_candidate,file_path):
    connexion = twitter_setup()
    L=get_candidate_queries(num_candidate,file_path)
    listeDeTweets=[]
    listeDeLikes=[]
    listeDeRetweets=[]
    for i in L:
        tweets = connexion.search(i,rpp=100)
        for tweet in tweets:
            listeDeTweets.append(tweet.text)
            listeDeLikes.append(tweet.retweet_count)
            listeDeRetweets.append(tweet.favorite_count)
    df=pd.DataFrame ( { 'text' : listeDeTweets,
                        'Likes' : listeDeLikes,
                        'Retweets' : listeDeRetweets
    })
    return df


def histogramme(num_candidate,file_path):
    df= get_tweets_from_candidates_search_queries(num_candidate,file_path)
    s=0
    for i in df['Likes']:
        s+=i
    NbreDeLikes = s
    s=0
    for i in df['Retweets']:
        s+=i
    NbreDeRetweets= s
    return ([NbreDeLikes,NbreDeRetweets])



def affichageLikes(num_candidate_1,file_path,num_candidate_2):
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    app.layout = html.Div(children=[
        html.H1(children='Nbre de tweets et de likes'),

        html.Div(children='''
            Analyse de tweets.
        '''),

        dcc.Graph(
            id='example-graph',
            figure={
                'data': [
                    {'x': ['Likes','Retweets'], 'y':histogramme(num_candidate_1,file_path), 'type': 'bar', 'name': 'candidat1'  },
                    {'x': ['Likes','Retweets'], 'y':histogramme(num_candidate_2,file_path), 'type': 'bar', 'name': 'candidat2'  }],
                'layout': {
                    'title': 'Dash Data Visualization'
                }
            }
        )
     ])

    if __name__ == '__main__':
        app.run_server(debug=True)
affichageLikes(1,'C:\\Users\\rimha\\PycharmProjects\\twitterPredictor\\CandidateData',2)