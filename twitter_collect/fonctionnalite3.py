import os
def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    keyword_file_name = 'keywords_candidate_' + str(num_candidate) + '.txt'
    hashtag_file_name = 'hashtag_candidate_' + str(num_candidate) + '.txt'
    path_keywords = os.path.join(file_path, keyword_file_name)
    path_hashtags = os.path.join(file_path, hashtag_file_name)
    try:
        keywords = open(path_keywords).readlines()
        hashtags = open(path_hashtags).readlines()
        return keywords+hashtags
    except IOError:
        print('The file cannot be opened, please check the given path / condidate number')

