from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError
import pandas


def max_keyword(data,num_candidat1,num_candidat2,file_path_):
    ## La personnalité la plus populaire
    liste1=get_candidate_queries(num_candidat1,file_path) ##liste des keywords et hashtags du candidat1
    liste2 = get_candidate_queries(num_candidat2, file_path)  ##liste des keywords et hashtags du candidat2
    s1=0 #liste des occurences des hashtags du 1er condidat pour chaque jour de la semaine
    s2=0
    l=['sun','mon','tue','wed','thu','fri','sat']
    for k in range(len(l)-1) :
        for i in data[l[k]+'nov'+str(k+11)+'00:00',l[k+1]+'nov'+str(k+12)+'00:00'].index[0]:
            t1=0 ##occurences des hashtags du 1er condidat pendant une journée (variable intermédiaire)
            l1=0
            for j in liste1:
                if not(i.find(j)) == -1:
                    t1+=1
            s1.append(t1)
            for j in liste2:
                if not(i.find(j)) == -1:
                    l1+=1
            s2.append(l1)
    return(s1,s2,l)

T=max_keyword(data,num_candidat1,num_candidat2,file_path_)
tfav = pd.Series(data=T[0], index=T[2])
tret = pd.Series(data=T[1], index=T[2])

# Likes vs retweets visualization:
tfav.plot(figsize=(16, 4), label="candidat1", legend=True)
tret.plot(figsize=(16, 4), label="candidat2", legend=True)

plt.show()

