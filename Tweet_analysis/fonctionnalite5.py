from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError
import pandas
def max_RT(data):
    rt_max = np.max(data['RTs'])
    rt = data[data.RTs == rt_max].index[0]

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))

def max_likes(data):
    rt_max = np.max(data['Likes'])
    rt = data[data.RTs == rt_max].index[0]

    # Max Likes:
    print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of likes: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))

def max_keyword(data,num_candidat1,num_candidat2,file_path_):
    ## La personnalité la plus populaire
    liste1=get_candidate_queries(num_candidat1,file_path) ##liste des keywords et hashtags du candidat1
    liste2 = get_candidate_queries(num_candidat2, file_path)  ##liste des keywords et hashtags du candidat2
    s1=0 #occurences des hashtags du 1er condidat
    s2=0
    for i in data.index[0]:
        for j in liste1:
            if not(i.find(j)) == -1:
                s1+=1
        for j in liste2:
            if not(i.find(j)) == -1:
                s2+=1
    maximum=max(s1,s2)
    if maximum == s1:
        nom_candidat = liste1[0]
    else :
        nom_candidat = liste2[0]
    # Max tweets:
    print("The candidate who has the more tweets : \n{}".format(nom_candidat)
    print("Number of tweets: {}".format(maximum))





