from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from twitter_collect.get_retweets_of_candidate import collect_by_user_id
from tweepy.error import RateLimitError
import json

def store_tweets(num_candidate,file_path,filename):
    connexion = twitter_setup()
    L=get_candidate_queries(num_candidate,file_path)

    list = []
    for i in L:
        tweets = connexion.search(i,rpp=100)
        for tweet in tweets:
            d = {}
            tweetText=tweet.text
            tweetId=tweet.id_str
            tweetDate=tweet.created_at
            d["tweetId"]=tweetId
            d["tweetText"]=tweetText
            d["tweetDate"]=tweetDate
            list.append(d)
    print(list)
    json.dump(list,filename)

def collect_by_user_id(user_id,filename):
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 200)
    l=[]
    for status in statuses:
        d = {}
        d['tweetRetweets']=status.retweet_count
        d['tweetId']=status.id_text
        d['tweetDate']=status.created_at
        l.append(d)
    json.dumps(l,filename)


