from textblob import TextBlob
from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def get_tweets_from_candidates_search_queries(num_candidate,file_path):
    Res=[] ##Liste des tweets concernant le candidat
    connexion = twitter_setup()
    L=get_candidate_queries(num_candidate,file_path)
    for i in L:
        tweets = connexion.search(i,rpp=100)
        for tweet in tweets:
            Res.append(tweet.text)
    return(Res)

def vocabulaire(num_candidate,file_path):
    Res=get_tweets_from_candidates_search_queries(num_candidate,file_path)
    SubjTweets=[]  ##la liste du vocab sujbectif concernant un candidat
    nltk.download('stopwords')
    for tweet in Res:
        tweetBlob=TextBlob(tweet)
        stop_words = set(stopwords.words('english'))
        word_tokens = tweetBlob.words
        tweetWords = [w for w in word_tokens if not w in stop_words]
        for i in range(len(tweetWords)):
            tweetWords[i]=tweetWords[i].lemmatize()
            word=TextBlob(tweetWords[i])
            if word.sentiment.subjectivity > 0.3:
                SubjTweets.append(tweetWords[i])
    return(SubjTweets)



