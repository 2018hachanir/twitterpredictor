from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError
def collect_by_user_id(user_id):
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 200)
    tweets_id=[]
    for status in statuses:
        tweets_id.append(status.id)
    return tweets_id

def get_retweets_of_candidate(user_id):
    tweets=collect_by_user_id(user_id)
    connexion = twitter_setup()
    try:
        for tweet in tweets:
            print(connexion.retweets(tweet).text)
    except RateLimitError:
        print('LOL')


