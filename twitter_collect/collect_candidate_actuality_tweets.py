from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError


def get_tweets_from_candidates_search_queries(num_candidate,file_path):
    connexion = twitter_setup()
    L=get_candidate_queries(num_candidate,file_path)
    for i in L:
        tweets = connexion.search(i,rpp=100)
        for tweet in tweets:
            print(tweet.text)

