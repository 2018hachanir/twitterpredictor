from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError


def get_replies_to_candidate(username):
    connexion = twitter_setup()
    tweets = connexion.search(q="to:"+username)
    for tweet in tweets:
        print(tweet.text)