import pandas

def pourcentage(data):
    nbPos=0## nbre de tweets positifs
    nbNeut=0
    nbNeg=0
    for tweet in data['tweet_textual_content']:
        tweetBlob= textBlob(tweet)
        if tweetBlob.sentiment.polarity > 0 :
            nbPos += 1
        elif tweetBlob.sentiment.polarity < 0  :
            nbNeg +=1
        else :
            nbNeut +=1
    print("Percentage of positive tweets: {}%".format(nbPos * 100 / len(data['tweet_textual_content'])))
    print("Percentage of neutral tweets: {}%".format(nbNeut * 100 / len(data['tweet_textual_content'])))
    print("Percentage de negative tweets: {}%".format(nbNeg * 100 / len(data['tweet_textual_content'])))
