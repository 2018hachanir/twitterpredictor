import pandas as pd

import numpy as np
import seaborn as sns

import matplotlib as plt

from textblob import TextBlob
from twitter_collect.fonctionnalite3 import get_candidate_queries
from twitter_collect.twitter_connection_setup import twitter_setup
from tweepy.error import RateLimitError
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def get_tweets_from_candidates_search_queries(num_candidate,file_path):
    Res=[] ##Liste des tweets concernant le candidat
    connexion = twitter_setup()
    L=get_candidate_queries(num_candidate,file_path)
    for i in L:
        tweets = connexion.search(i,rpp=100)
        for tweet in tweets:
            Res.append(tweet.text)
    return(Res)
## là on a obntenu la liste de tweets


def polarite(num_candidate,file_path):
    data=get_tweets_from_candidates_search_queries(num_candidate,file_path)
    tweetPolarity=[0 for j in range(20)]
    for tweet in data:
        tweetBlob = TextBlob(tweet)
        j=0
        test=False
        while test==False and j<20:
            print(tweetBlob.sentiment.polarity)
            if (tweetBlob.sentiment.polarity >= -1+j*0.1) and (tweetBlob.sentiment.polarity <-0.9+j*0.1):

                tweetPolarity[j]+=1
                test=True
            j+=1
    return tweetPolarity
# cette fontion renvoie une liste du nbre de tweets correspondants à une polarité dans des intervalles
# définis par linspace : [-1,-0.9] +j*0.1

def histogramme (num_candidate,file_path):


    tt = polarite(num_candidate, file_path)
    print('debut de la fontion')
    L=np.asarray(tt)
    print('L lu')
    df2 = pd.DataFrame({ 'Polarity' : np.linspace(-1,1,20),
                         'Nombre_de_tweets' : L })

    sns.set(style="darkgrid")

# Load an example dataset with long-form data

    # Plot the responses for different events and regions
    sns.lineplot(x=df2["Polarity"], y=df2["Nombre_de_tweets"],

             data=df2)
    plt.pyplot.show()

histogramme (1,'C:\\Users\\rimha\\PycharmProjects\\twitterPredictor\\CandidateData')